using System.Collections.Generic;
using _2359.Apis;
using _2359.Entities;
using _2359.Enums;
using _2359.Models;
using _2359.Repository.Interface;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace _2359UnitTesting
{
    public class ShapeUnitTest
    {
        private Mock<IGenericRepository<Shape>> shapeMock;
        private Mock<IGenericRepository<Side>> sideMock;
        private Mock<ILoggerFactory> loggerFactory; 
        
        public ShapeUnitTest()
        {
            this.shapeMock = new Mock<IGenericRepository<Shape>>();
            this.sideMock = new Mock<IGenericRepository<Side>>();
            this.loggerFactory = new Mock<ILoggerFactory>();
        }

        private void SetupReturnSuccess()
        {
            this.shapeMock.Setup(s => s.Add(It.IsAny<Shape>())).ReturnsAsync(new Shape());
            this.sideMock.Setup(s => s.Add(It.IsAny<Side>())).ReturnsAsync(new Side());
            
            this.shapeMock.Setup(s => s.Update(It.IsAny<Shape>())).ReturnsAsync(new Shape());
            this.sideMock.Setup(s => s.Update(It.IsAny<Side>())).ReturnsAsync(new Side());
            
            this.shapeMock.Setup(s => s.Remove(It.IsAny<Shape>())).ReturnsAsync(new Shape());
            this.sideMock.Setup(s => s.Remove(It.IsAny<Side>())).ReturnsAsync(new Side());
        }

        [Fact]
        public async void AddShapeReturnSuccess()
        {
            this.SetupReturnSuccess();
            ShapeApiController controller = new ShapeApiController(shapeMock.Object, sideMock.Object, loggerFactory.Object);
            
            Circle circle = new Circle(ShapeEnum.Circle);
            circle.Sides = new List<SideModel>();
            circle.Sides.Add(new SideModel());
            
            var obj = await controller.CreateShape(circle); 
            
            Assert.NotNull(obj);
        }
        
        [Fact]
        public async void UpdateShapeReturnSuccess()
        {
            this.SetupReturnSuccess();
            ShapeApiController controller = new ShapeApiController(shapeMock.Object, sideMock.Object, loggerFactory.Object);
            
            Circle circle = new Circle(ShapeEnum.Circle);
            circle.Sides = new List<SideModel>();
            circle.Sides.Add(new SideModel());
            
            var obj = await controller.UpdateShape(circle); 
            
            Assert.NotNull(obj);
        }
        
        [Fact]
        public async void DeleteShapeReturnSuccess()
        {
            this.SetupReturnSuccess();
            ShapeApiController controller = new ShapeApiController(shapeMock.Object, sideMock.Object, loggerFactory.Object);
            var obj = await controller.DeleteShape(1); 
            
            Assert.NotNull(obj);
        }
    }
}