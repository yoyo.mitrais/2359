using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using _2359.Infrastructure;
using _2359.Entities;
using _2359.Models;
using _2359.Repository.Interface;
using System.Threading.Tasks;
using _2359.Utility;
using System.Linq;

namespace _2359.Apis
{
    [Route("api/shape")]
    public class ShapeApiController : Controller
    {
        private IGenericRepository<Shape> shapeRepo;
        private IGenericRepository<Side> sideRepo;

        private readonly ILogger logger;

        public ShapeApiController(IGenericRepository<Shape> shapeRepo, IGenericRepository<Side> sideRepo, ILoggerFactory loggerFactory) {
            this.shapeRepo = shapeRepo;
            this.sideRepo = sideRepo;

            logger = loggerFactory.CreateLogger(nameof(ShapeApiController));
        }

        #region get 

        // GET api/shape
        [HttpGet("all/")]
        [NoCache]
        [ProducesResponseType(typeof(List<ShapeModel>), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> Shapes()
        {
            try
            {
                List<ShapeModel> shapeModels = new List<ShapeModel>();

                var shapes = await this.shapeRepo.GetAll();
                foreach (var shape in shapes)
                {
                    var shapeModel = EntityConverter.ConstructShapeModel(shape);
                    var sides = sideRepo.GetWhere(s => s.ShapeId == shape.Id).GetAwaiter().GetResult();

                    List<SideModel> sideModels = new List<SideModel>();

                    foreach (var side in sides)
                    {
                        sideModels.Add(EntityConverter.ConstructSideModel(side));
                    }

                    shapeModel.Sides = sideModels;
                    shapeModels.Add(shapeModel);
                }

                return Ok(shapes.OrderBy(s => s.Name).ToList());
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        // GET api/shape/page/10/10
        [HttpGet("page/{skip}/{take}")]
        [NoCache]
        [ProducesResponseType(typeof(List<ShapeModel>), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> GetShapesPage(int skip, int take)
        {
            try
            {
                List<ShapeModel> shapeModels = new List<ShapeModel>();
                var pagingResult = await shapeRepo.GetShapesPageAsync(skip, take);

                foreach (var shape in pagingResult.Records)
                {
                    var shapeModel = EntityConverter.ConstructShapeModel(shape);
                    var sides = sideRepo.GetWhere(s => s.ShapeId == shape.Id).GetAwaiter().GetResult();

                    List<SideModel> sideModels = new List<SideModel>();

                    foreach (var side in sides)
                    {
                        sideModels.Add(EntityConverter.ConstructSideModel(side));
                    }

                    shapeModel.Sides = sideModels;
                    shapeModels.Add(shapeModel);
                }

                Response.Headers.Add("X-InlineCount", pagingResult.TotalRecords.ToString());
                return Ok(shapeModels);
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        // GET api/shape/5
        [HttpGet("{id}", Name = "GetShapeRoute")]
        [NoCache]
        [ProducesResponseType(typeof(ShapeModel), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> GetShape(int id)
        {
            try
            {
                var shape = await shapeRepo.GetById(id);
                var shapeModel = EntityConverter.ConstructShapeModel(shape);

                List<SideModel> sideModels = new List<SideModel>();
                var sides = sideRepo.GetWhere(s => s.ShapeId == shape.Id).GetAwaiter().GetResult();

                foreach (var side in sides)
                {
                    sideModels.Add(EntityConverter.ConstructSideModel(side));
                }

                shapeModel.Sides = sideModels;

                return Ok(shapeModel);
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        #endregion 

        #region create 

        // POST api/shape
        [HttpPost("CreateShapeBase/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateShape([FromBody] ShapeModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructShapeEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateCircle/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateCircle([FromBody] Circle model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructCircleEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateEllipse/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateEllipse([FromBody] Ellipse model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructEllipseEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateKite/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateKite([FromBody] Kite model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructKiteEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateParallelogram/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateParallelogram([FromBody] Parallelogram model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructParallelogramEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }


        [HttpPost("CreateRectangle/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateRectangle([FromBody] Rectangle model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructRectangleEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateRhombus/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateRhombus([FromBody] Rhombus model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructRhombusEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateSquare/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateSquareModel([FromBody] Square model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructSquareEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateTrapezium/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateTrapeziumModel([FromBody] Trapezium model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructTrapeziumEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPost("CreateTriangle/")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> CreateTriangleModel([FromBody] Triangle model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var shape = EntityConverter.ConstructTriangleEntity(model);
                var newShape = await shapeRepo.Add(shape);

                if (newShape == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                model.Id = shape.Id;

                return CreatedAtRoute("GetShapeRoute", new { id = newShape.Id },
                        new ApiResponse { Status = true, ShapeModel = model });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        #endregion

        #region update 

        // PUT api/shape/5
        [HttpPut("UpdateBaseShape/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateShape([FromBody] ShapeModel shape)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructShapeEntity(shape));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = shape });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        // PUT api/shape/5
        [HttpPut("UpdateCircle/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateCircle([FromBody] Circle shape)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructCircleEntity(shape));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = shape });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateEllipse/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateEllipse([FromBody] Ellipse shape)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructEllipseEntity(shape));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = shape });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateKite/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateKite([FromBody] Kite kite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructKiteEntity(kite));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = kite });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateParallelogram/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateParallelogram([FromBody] Parallelogram parallelogram)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructParallelogramEntity(parallelogram));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = parallelogram });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateRectangle/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateRectangle([FromBody] Rectangle rectangle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructRectangleEntity(rectangle));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = rectangle });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateRhombus/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateRhombus([FromBody] Rhombus rhombus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructRhombusEntity(rhombus));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = rhombus });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateSquare/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateSquare([FromBody] Square square)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructSquareEntity(square));
                List<int> list = new List<int>();
                list.Sum();
                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = square });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateTrapezium/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateTrapezium([FromBody] Trapezium trapezium)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructTrapeziumEntity(trapezium));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = trapezium });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        [HttpPut("UpdateTriangle/")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> UpdateTriangle([FromBody] Triangle triangle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiResponse { Status = false, ModelState = ModelState });
            }

            try
            {
                var result = await shapeRepo.Update(EntityConverter.ConstructTriangleEntity(triangle));

                if (result == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }

                return Ok(new ApiResponse { Status = true, ShapeModel = triangle });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        #endregion

        #region delete

        // DELETE api/shape/5
        [HttpDelete("delete/{id}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<ActionResult> DeleteShape(int id)
        {
            try
            {
                var status = await shapeRepo.Remove(await shapeRepo.GetById(id));
                if (status == null)
                {
                    return BadRequest(new ApiResponse { Status = false });
                }
                return Ok(new ApiResponse { Status = true });
            }
            catch (Exception exp)
            {
                logger.LogError(exp.Message);
                return BadRequest(new ApiResponse { Status = false });
            }
        }

        #endregion 

    }
}
