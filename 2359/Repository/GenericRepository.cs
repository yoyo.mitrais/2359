using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using _2359.Entities;
using _2359.Models;
using _2359.Repository.Interface;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace _2359.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : EntityBase
    {

        #region Fields

        protected ShapeDbContext context;
        private readonly ILogger logger;

        #endregion

        public GenericRepository(ShapeDbContext context, ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger("GenericRepository");
        }

        #region Public Methods

        public Task<T> GetById(int id) => context.Set<T>().FindAsync(id);

        public Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate)
            => context.Set<T>().FirstOrDefaultAsync(predicate);

        public async Task<T> Add(T entity)
        {
            this.context.Add(entity);
            
            if (entity.GetType() == typeof(Shape))
            {
                foreach (var side in (entity as Shape).Sides)
                {
                    side.ShapeId = entity.Id;
                    this.context.Add(side);
                }
            }

            int result = 0; 
            try
            {
                 result = await this.context.SaveChangesAsync();
            }
            catch (System.Exception exp)
            {
                logger.LogError($"Error in {nameof(Add)}: " + exp.Message);
                return null;
            }
            
            return entity;
        }

        public async Task<T> Update(T entity)
        {
            try
            {
              // In case AsNoTracking is used
              this.context.Entry(entity).State = EntityState.Modified;
              
              if (entity.GetType() == typeof(Shape)) 
              {
                 var removeSides = this.context.Side.Where(s => s.ShapeId == entity.Id).ToList();
                 foreach (var sdr in removeSides)
                     this.context.Remove(sdr);

                 foreach (var side in (entity as Shape).Sides.Where(sr => !removeSides.Contains(sr)))
                 {
                      side.ShapeId = entity.Id;
                      this.context.Add(side);
                 }
              }
               
              var res = (await context.SaveChangesAsync() > 0 ? true : false);
            }
            catch (Exception exp)
            {
                logger.LogError($"Error in {nameof(Update)}: " + exp.Message);
                return null;
            }
            return entity;
        }
        
        public async Task<T>  Remove(T entity)
        {
            try
            {
                context.Set<T>().Remove(entity);
                
                if (entity.GetType() == typeof(Shape)) 
                {
                    (entity as Shape).Sides = context.Side.Where(s => s.ShapeId == entity.Id).ToList();
                    foreach (var side in (entity as Shape).Sides)
                        this.context.Remove(side);
                }

                await context.SaveChangesAsync();
            }
            catch (Exception exp)
            {
                logger.LogError($"Error in {nameof(Remove)}: " + exp.Message);
                return null;
            }

            return entity;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await context.Set<T>().Where(predicate).ToListAsync();
        }

        public Task<int> CountAll() => context.Set<T>().CountAsync();

        public Task<int> CountWhere(Expression<Func<T, bool>> predicate) 
            => context.Set<T>().CountAsync(predicate);
        
        public async Task<PagingResult<Shape>> GetShapesPageAsync(int skip, int take)
        {
            var totalRecords = await context.Shape.CountAsync();
            var results = await context.Shape
                .OrderBy(c => c.Category)
                .Include(c => c.Base)
                .Include(c => c.Height)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
            
            return new PagingResult<Shape>(results, totalRecords);
        }
        
        public async Task<PagingResult<Side>> GetSidesPageAsync(int skip, int take)
        {
            var totalRecords = await context.Shape.CountAsync();
            var results = await context.Side
                .OrderBy(c => c.ShapeId)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
            
            return new PagingResult<Side>(results, totalRecords);
        }

        #endregion

    }
}