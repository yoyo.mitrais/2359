using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _2359.Entities;
using _2359.Enums;
using _2359.Models;
using _2359.Repository.Interface;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace _2359.Repository
{
    public class ShapeDbSeeder
    {
        private readonly ILogger logger;
        private IGenericRepository<Shape> shapeRepo;
        private IGenericRepository<Side> sideRepo; 
        
        public ShapeDbSeeder(IGenericRepository<Shape> shapeRepo, IGenericRepository<Side> sideRepo, ILoggerFactory loggerFactory)
        {
            logger = loggerFactory.CreateLogger("InsertShapeDbSeeder");
            this.shapeRepo = shapeRepo;
            this.sideRepo = sideRepo;
        }

        public async Task SeedAsync(IServiceProvider serviceProvider)
        {
            // remove all data first
            var toBeRemoved = await shapeRepo.GetAll();
            foreach (var shape in toBeRemoved)
            {
               await shapeRepo.Remove(shape);
            }

            await InsertShapeDb(shapeRepo);
        }
        
        public Task InsertShapeDb(IGenericRepository<Shape> shapeRepo)
        {
            try
            {
                foreach (var shape in this.GetShapes())
                {
                    shapeRepo.Add(shape);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error in {nameof(ShapeDbSeeder)}: " + ex.Message);
            }

            return Task.FromResult<object>(null);
        }

        private List<Shape> GetShapes()
        {
            List<Shape> shapes = new List<Shape>();

            Shape shape = new Shape();
            shape.Name = ShapeEnum.Rhombus.ToString();
            shape.Base = 0;
            shape.Category = (int)ShapeEnum.Rhombus;
            shape.Height = 10;
            shape.Sides = new List<Side>();
            Side side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            shapes.Add(shape);


            shape = new Shape();
            shape.Name = ShapeEnum.Square.ToString();
            shape.Base = 10;
            shape.Height = 10;
            shape.Sides = new List<Side>();
            shapes.Add(shape);

            shape = new Shape();
            shape.Name = ShapeEnum.Trapezium.ToString();
            shape.Category = (int)ShapeEnum.Trapezium;
            shape.Sides = new List<Side>();
            shape.Base2 = 7;
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 3;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 7;
            shape.Sides.Add(side);
            shapes.Add(shape);

            shape = new Shape();
            shape.Name = ShapeEnum.Triangle.ToString();
            shape.Category = (int)ShapeEnum.Triangle;
            shape.Sides = new List<Side>();
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 4;
            shape.Sides.Add(side);
            side = new Side();
            side.Value = 6;
            shape.Sides.Add(side);
            shapes.Add(shape);

            shape = new Shape();
            shape.Name = ShapeEnum.Circle.ToString();
            shape.Category = (int) ShapeEnum.Circle;
            shape.R = 10;
            shape.X = 5; 
            shape.Sides = new List<Side>(); 
            shapes.Add(shape);
            
            shape = new Shape();
            shape.Name = ShapeEnum.Ellipse.ToString();
            shape.Category = (int)ShapeEnum.Ellipse;
            shape.X = 5;
            shape.Y = 10;
            shape.Aaxis = 10;
            shape.Baxis = 20;
            shape.Sides = new List<Side>(); 
            shapes.Add(shape);

            shape = new Shape();
            shape.X = 10;
            shape.Y = 20;
            shape.Name = ShapeEnum.Kite.ToString();
            shape.Category = (int)ShapeEnum.Kite;
            shape.Sides = new List<Side>(); 
            shapes.Add(shape);

            shape = new Shape();
            shape.Base = 5;
            shape.Height = 10;
            shape.Name = ShapeEnum.Rectangle.ToString();
            shape.Category = (int)ShapeEnum.Rectangle;
            shape.Sides = new List<Side>(); 
            shapes.Add(shape);

            return shapes;
        }
    }
}