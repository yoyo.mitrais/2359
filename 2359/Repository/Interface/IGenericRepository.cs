using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using _2359.Entities;
using _2359.Models;
using _2359.Entities;
using _2359.Models;

namespace _2359.Repository.Interface
{
    public interface IGenericRepository<T> where T : EntityBase
    {
 
        Task<T> GetById(int id);
        Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate);
 
        Task<T> Add(T entity);
        Task<T> Update(T entity);
        Task<T> Remove(T entity);
 
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
 
        Task<int> CountAll();
        Task<int> CountWhere(Expression<Func<T, bool>> predicate);

        Task<PagingResult<Shape>> GetShapesPageAsync(int skip, int take);

        Task<PagingResult<Side>> GetSidesPageAsync(int skip, int take);

    }
}