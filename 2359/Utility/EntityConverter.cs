using System;
using System.Collections.Generic;
using _2359.Entities;
using _2359.Enums;
using _2359.Models;

namespace _2359.Utility
{
    public class EntityConverter
    {
        public static ShapeModel ConstructShapeModel(Shape shape, List<SideModel> sideModels = null)
        {
            ShapeModel model = null; 
            
            switch(shape.Category.GetValueOrDefault()) 
            {
                case (int) ShapeEnum.Circle:
                    model = new Circle(ShapeEnum.Circle); 
                    break; 
                case (int) ShapeEnum.Ellipse:
                    model = new Ellipse(ShapeEnum.Ellipse); 
                    break; 
                case (int) ShapeEnum.Kite:
                    model = new Circle(ShapeEnum.Kite); 
                    break; 
                case (int) ShapeEnum.Parallelogram:
                    model = new Parallelogram(ShapeEnum.Parallelogram); 
                    break; 
                case (int) ShapeEnum.Rectangle:
                    model = new Rectangle(ShapeEnum.Rectangle); 
                    break; 
                case (int) ShapeEnum.Rhombus:
                    model = new Rhombus(ShapeEnum.Rhombus); 
                    break; 
                case (int) ShapeEnum.Square:
                    model = new Square(ShapeEnum.Square); 
                    break; 
                case (int) ShapeEnum.Trapezium:
                    model = new Trapezium(ShapeEnum.Trapezium); 
                    break; 
                case (int) ShapeEnum.Triangle:
                    model = new Triangle(ShapeEnum.Triangle); 
                    break;
                default: model = new AbstractShapeModel(ShapeEnum.AbstractShape);
                    break;
            }

            model.Id = shape.Id;
            model.Name = shape.Name;
            model.Base = shape.Base.GetValueOrDefault();
            model.Height = shape.Height.GetValueOrDefault();

            if (sideModels != null)
                model.Sides = sideModels;
            else model.Sides = null; 
            
            model.X = shape.X.GetValueOrDefault();
            model.Y = shape.Y.GetValueOrDefault();

            return model; 
        }
        
        public static Shape ConstructShapeEntity(ShapeModel shapeModel)
        {
            Shape shape = new Shape();

            shape.Id = shapeModel.Id;
            shape.Name = shapeModel.Name;
            shape.Base = shapeModel.Base;
            shape.Height = shapeModel.Height;
            
            shape.X = shapeModel.X;
            shape.Y = shapeModel.Y;
            
            List<Side> sides = new List<Side>();

            if (shapeModel.Sides != null)
            {
                foreach (var s in shapeModel.Sides)
                {
                    var side = ConstructSideEntity(s);
                    side.ShapeId = shapeModel.Id;
                    sides.Add(side);
                }
            }

            shape.Sides = sides; 

            return shape; 
        }

        public static SideModel ConstructSideModel(Side side)
        {
            SideModel model = new SideModel();

            model.ShapeId = side.ShapeId;
            model.Value = side.Value;
            model.Id = side.Id;
            
            return model; 
        }
        
        public static Side ConstructSideEntity(SideModel model)
        {
            Side side  = new Side();

            side.ShapeId = model.ShapeId;
            side.Value = model.Value;
            side.Id = model.Id;
            side.Id = model.Id;
            
            return side; 
        }

        public static Shape ConstructCircleEntity(Circle model)
        {
            var md = ConstructShapeEntity(model);
            md.R = model.R;
            return md;
        }

        public static Shape ConstructEllipseEntity(Ellipse model)
        {
            var md = ConstructShapeEntity(model);
            md.Baxis = model.BAxis;
            md.Aaxis = model.AAxis;
            return md;
        }
        public static Shape ConstructKiteEntity(Kite model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }
        public static Shape ConstructRectangleEntity(Rectangle model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }

        public static Shape ConstructRhombusEntity(Rhombus model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }

        public static Shape ConstructSquareEntity(Square model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }

        public static Shape ConstructTrapeziumEntity(Trapezium model)
        {
            var md = ConstructShapeEntity(model);
            md.Base2 = model.Base2;
            return md;
        }

        public static Shape ConstructTriangleEntity(Triangle model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }

        public static Shape ConstructParallelogramEntity(Parallelogram model)
        {
            var md = ConstructShapeEntity(model);
            return md;
        }
    }
}