﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace _2359.Entities
{
    public partial class ShapeDbContext : DbContext
    {
        public virtual DbSet<Shape> Shape { get; set; }
        public virtual DbSet<Side> Side { get; set; }
        
        public ShapeDbContext (DbContextOptions<ShapeDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite(@"Data Source=ShapeSql.sqlite");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shape>(entity =>
            {
                entity.ToTable("shape");

                entity.HasIndex(e => e.Id)
                    .HasName("shape_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aaxis)
                    .HasColumnName("aaxis")
                    .HasColumnType("double");

                entity.Property(e => e.Base)
                    .HasColumnName("base")
                    .HasColumnType("double");

                entity.Property(e => e.Base2)
                    .HasColumnName("base2")
                    .HasColumnType("double");

                entity.Property(e => e.Baxis)
                    .HasColumnName("baxis")
                    .HasColumnType("double");

                entity.Property(e => e.Category).HasColumnName("category");

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasColumnType("double");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.R)
                    .HasColumnName("r")
                    .HasColumnType("double");

                entity.Property(e => e.X)
                    .HasColumnName("x")
                    .HasColumnType("double");

                entity.Property(e => e.Y)
                    .HasColumnName("y")
                    .HasColumnType("double");
            });

            modelBuilder.Entity<Side>(entity =>
            {
                entity.ToTable("side");

                entity.HasIndex(e => e.Id)
                    .HasName("side_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ShapeId).HasColumnName("shape_id");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("double");
            });

            modelBuilder.Entity<Shape>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<Side>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            base.OnModelCreating(modelBuilder);
        }
    }
}
