﻿using System;
using System.Collections.Generic;

namespace _2359.Entities
{
    public partial class Shape : EntityBase
    {
        public double? Base { get; set; }
        public double? Height { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public double? Base2 { get; set; }
        public int? Category { get; set; }
        public double? Aaxis { get; set; }
        public double? Baxis { get; set; }
        public double? R { get; set; }
        
        public string Name { get; set; }
        
        public List<Side> Sides { get; set; }
    }
}
