﻿using System;
using System.Collections.Generic;

namespace _2359.Entities
{
    public partial class Side : EntityBase
    {
        public int ShapeId { get; set; }
        public double Value { get; set; }
    }
}
