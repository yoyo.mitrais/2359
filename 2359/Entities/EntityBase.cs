using System.ComponentModel.DataAnnotations;

namespace _2359.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }
    }
}