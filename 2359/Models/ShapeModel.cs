using System.Collections.Generic;
using _2359.Enums;
using _2359.Models;
using _2359.Models.Interface;

namespace _2359.Models
{
    public class ShapeModel : BaseModel, IShapeMethod
    {
        private string name;
        private double x;
        private double y;
        private double base1;
        private double height;
        
        private List<SideModel> sides;
        
        private ShapeEnum category;

        public ShapeModel()
        {
        }

        public ShapeModel(ShapeEnum category)
        {
            this.category = category;
        }
        
        public double X
        {
            get => x;
            set => x = value;
        }

        public double Y
        {
            get => y;
            set => y = value;
        }

        public double Base
        {
            get => base1;
            set => base1 = value;
        }

        public double Height
        {
            get => height;
            set => height = value;
        }

        public ShapeEnum Category => category;

        public List<SideModel> Sides
        {
            get => sides;
            set => sides = value;
        }
        public string Name { get => name; set => name = value; }

        public virtual double CalculatePerimeter()
        {
            throw new System.NotImplementedException();
        }

        public virtual double CalculateSize()
        {
            throw new System.NotImplementedException();
        }
    }
}