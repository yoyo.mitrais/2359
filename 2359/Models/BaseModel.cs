namespace _2359.Models
{
    public class BaseModel
    {
        private int id;

        public int Id
        {
            get => id;
            set => id = value;
        }
    }
}