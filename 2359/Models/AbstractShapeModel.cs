﻿using _2359.Enums;
using System;

namespace _2359.Models
{
    // Todo : this is abstract shape model need to be implemented 
    public class AbstractShapeModel : ShapeModel
    {
        public AbstractShapeModel(ShapeEnum category) : base(category)
        {
        }
    }
}