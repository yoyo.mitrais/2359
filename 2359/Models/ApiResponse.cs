﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2359.Models;

namespace _2359.Models
{
    public class ApiResponse
    {
        public bool Status { get; set; }
        public ModelStateDictionary ModelState { get; set; }
        
        public ShapeModel ShapeModel { get; set; }
    }
}
