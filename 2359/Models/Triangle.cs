using System;
using System.Collections.Generic;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Triangle : ShapeModel
    {

        public Triangle(ShapeEnum category) : base(category) 
        {
          
        }
        
        public override double CalculateSize()
        {
            return 0.5 * this.Base * this.Height; 
        }

        public override double CalculatePerimeter()
        {
            if (Sides.Count != 3) throw new InvalidOperationException("Invalid Triangle Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}