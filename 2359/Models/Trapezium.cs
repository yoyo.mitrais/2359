using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Trapezium : ShapeModel
    {
        private double base2; 
        
        public Trapezium(ShapeEnum category) : base(category)
        {
        }
        
        public double Base2
        {
            get => base2;
            set => base2 = value;
        }

        public override double CalculateSize()
        {
            return 0.5 * (this.Base + this.base2) * this.Height;
        }

        public override double CalculatePerimeter()
        {
            if (this.Sides.Count != 4 ) throw new InvalidOperationException("Invalid Trapezium Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}