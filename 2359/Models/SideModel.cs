namespace _2359.Models
{
    public class SideModel : BaseModel
    {
        private int shapeId;
        private double side_size;

        public int ShapeId
        {
            get => shapeId;
            set => shapeId = value;
        }

        public double Value
        {
            get => side_size;
            set => side_size = value;
        }
    }
}