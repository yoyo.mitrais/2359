using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Circle : ShapeModel
    {
        private double r; 
        
        public Circle(ShapeEnum category) : base(category)
        {
        }

        public double R
        {
            get => r;
            set => r = value;
        }

        public override double CalculateSize()
        {
            return Math.PI * (this.r * this.r);
        }

        public override double CalculatePerimeter()
        {
            if (this.Sides.Count != 1 ) throw new InvalidOperationException("Invalid Ellipse Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}