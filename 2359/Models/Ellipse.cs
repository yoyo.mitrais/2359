using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Ellipse : ShapeModel
    {
        private double aAxis;
        private double bAxis; 
        
        public Ellipse(ShapeEnum category) : base(category)
        {
        }

        public double AAxis
        {
            get => aAxis;
            set => aAxis = value;
        }

        public double BAxis
        {
            get => bAxis;
            set => bAxis = value;
        }

        public override double CalculateSize()
        {
            return Math.PI * this.aAxis * this.bAxis;
        }

        public override double CalculatePerimeter()
        {
            if (this.Sides.Count != 1 ) throw new InvalidOperationException("Invalid Ellipse Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}