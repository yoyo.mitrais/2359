using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Rhombus : ShapeModel
    {
        public Rhombus(ShapeEnum category) : base(category)
        {
        }

        public override double CalculateSize()
        {
            return 0.5 * (this.Base * this.Height);
        }

        public override double CalculatePerimeter()
        {
            if (this.Sides.Count != 4 ) throw new InvalidOperationException("Invalid Rhombus Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}