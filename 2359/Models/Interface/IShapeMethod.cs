﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2359.Models.Interface
{
    interface IShapeMethod
    {
        double CalculateSize();
        double CalculatePerimeter();
    }
}
