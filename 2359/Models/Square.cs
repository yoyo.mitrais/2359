using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Square : ShapeModel
    {
        public Square(ShapeEnum category) : base(category)
        {
        }

        public override double CalculateSize()
        {
            return this.Base * this.Height;
        }

        public override double CalculatePerimeter()
        {
            if (Sides.Count != 4 ) throw new InvalidOperationException("Invalid Square Sides");
            return this.Sides.Sum(s => s.Value);
        }
    }
}