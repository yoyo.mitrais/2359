using System;
using System.Linq;
using _2359.Enums;
using _2359.Models;

namespace _2359.Models
{
    public class Kite : ShapeModel
    {
        public Kite(ShapeEnum category) : base(category)
        {
        }

        public override double CalculateSize()
        {
            return (this.Base * this.Height) * 0.5;
        }

        public override double CalculatePerimeter()
        {
            if (this.Sides.Count != 4 ) throw new InvalidOperationException("Invalid Kite Sides");
            return this.Sides.Sum(s => s.Value);;
        }
    }
}