namespace _2359.Enums
{
    public enum ShapeEnum
    {
        AbstractShape = 0,
        Triangle = 1, 
        Square = 2, 
        Rectangle = 3, 
        Parallelogram = 4, 
        Rhombus = 5, 
        Kite = 6, 
        Trapezium = 7, 
        Circle = 8, 
        Ellipse = 9
    }
}