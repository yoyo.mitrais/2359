import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface ShapeModel {
    name: string;
    x: number;
    y: number;
    base: number;
    height: number;
    category: number;
    aaxis: number;
    baxis: number;
    base2: number;
}

@Component
export default class FetchDataComponent extends Vue {
    shapes: ShapeModel[] = [];

    mounted() {
        fetch('/api/shape/all')
            .then(response => response.json() as Promise<ShapeModel[]>)
            .then(data => {
                this.shapes = data;
               // console.log(JSON.stringify(this.shapes));
            });
    }
}
